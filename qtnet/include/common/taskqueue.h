﻿#ifndef PROCQUEUE_H
#define PROCQUEUE_H

#include <QMutex>
#include <QRunnable>
#include <QThreadPool>
#include <QQueue>
#include <QSemaphore>


#include "qnet_global.h"
class TaskEntry;

/**
* 说明：    处理队列实现。
* 作者：    pc0de
* 日期：    2018-12-23
*/
class QNETSHARED_EXPORT TaskQueue
        :public QRunnable
{
public:
    /**
    *  @brief 构造方法。
    *  @param maxSize 最大长度。
    *  @param autoDel 是否自动删除实体。
    */
    TaskQueue(int maxSize, bool autoDel = true);
    virtual ~TaskQueue();
    /**
    *  @brief 加入处理队列入口实体。
    *  @param  entry 处理实例。
    *  @return 成功加入返回true。
    */
    bool add(TaskEntry *entry);
    /**
    *  @brief   销毁队列，队列全部执行完毕或取消完毕后返回。
    */
    void cancel();


    /**
    * @brief 获取未处理队列大小。
    * @return 返回未处理的队列大小。
    */
    int size() const;
private:
    /**
    * @brief 执行逻辑方法。
    */
    void excute();

private:
    // 消息队列。
    QQueue<TaskEntry*> m_queue;
    // 队列访问互斥锁。
    QMutex m_muxtex;
    // 信号量。
    QSemaphore m_semaphore;
    //是否取消。
    bool m_canceled;
    //是否正在处理。
    bool m_procing;
    //最大队列大小。
    int m_maxSize;
    // 是否自动删除实体。
    bool m_autoDel;

protected: // Runnable实现。
    void run();

};
#endif // PROCQUEUE_H
