﻿#ifndef TASKENTRY_H
#define TASKENTRY_H

#include "qnet_global.h"
/**
* 说明：    处理实体抽象。
* 作者：    pc0de
* 日期：    2018-12-23
*/
class QNETSHARED_EXPORT TaskEntry
{
public:
    // 构造
    explicit TaskEntry();
    //析构方法
    virtual ~TaskEntry();
    //处理消息通知。
    virtual void onProc() = 0 ;
    // 取消消息通知。
    virtual void onDiscard() = 0 ;
};

#endif // TASKENTRY_H




