﻿#ifndef QTSOCKET_NETREADING_H
#define QTSOCKET_NETREADING_H
#include <QByteArray>
#include <QHostAddress>

#include "qnet_global.h"
QNET_NAMESPACE_BEGIN
/**
 * @brief The NetReading class 表示网络数据读取抽象类。
 */
class QNETSHARED_EXPORT NetReading
{
public:
    NetReading();
    virtual ~NetReading();
    /**
     * @brief onProc 数据处理。
     * @param arry
     */
    virtual void onProc(const QByteArray &arry) = 0;

    QHostAddress host() const;
    void setHost(const QHostAddress &host);

    quint16 port() const;
    void setPort(const quint16 &port);
private:
    NetReading(const NetReading &src);
private:
    QHostAddress m_host;
    quint16 m_port;
};

QNET_NAMESPACE_END
#endif // NETWORKREADING_H
