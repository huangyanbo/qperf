﻿#ifndef QNET_GLOBAL_H
#define QNET_GLOBAL_H

#include <QtCore/qglobal.h>
#define QNET_NAMESPACE QtSocket
#define QNET_NAMESPACE_BEGIN namespace QNET_NAMESPACE{

#define QNET_NAMESPACE_END }

#define QNET_USING_NAMESPACE using namespace QNET_NAMESPACE;

#if defined(QNET_LIBRARY)
#  define QNETSHARED_EXPORT Q_DECL_EXPORT
#else
#if defined(QNET_BUILD)
    #define QNETSHARED_EXPORT Q_DECL_EXPORT
#else
    #define QNETSHARED_EXPORT Q_DECL_IMPORT
#endif // QNET_BUILD
#endif //QNET_LIBRARY

#endif // QNET_GLOBAL_H
