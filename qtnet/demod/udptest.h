﻿#ifndef UDPTEST_H
#define UDPTEST_H

#include <QHostAddress>
#include <QObject>
#include <net/netreading.h>

namespace QNET_NAMESPACE {
    class UdpChannel;
}
QNET_USING_NAMESPACE

/**
 * @brief The UdpTest class 测试udp会话。
 */
class UdpTest : public QObject,
        public NetReading
{
    Q_OBJECT
public:
    explicit UdpTest(QObject *parent = nullptr);
    /**
     * @brief start 启动UdpSession。
     * @param remoteIP
     * @param remotePort
     * @param localIP
     * @param localPort
     * @return
     */
    bool start(QHostAddress &remoteIP, quint16 remotePort, QHostAddress &localIP, quint16 localPort);
    // NetworkReading interface
public:
    void onProc(const QByteArray &arry) override;
    // QObject interface
protected:
    void timerEvent(QTimerEvent *event) override;
signals:

public slots:

private:
    QtSocket::UdpChannel *m_session;
    int m_timerID; // 计时器ID。


};

#endif // UDPTEST_H
