#-------------------------------------------------
#
# Project created by QtCreator 2020-12-16T16:14:38
#
#-------------------------------------------------

QT       -= gui
QT += network
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include($$PWD/include/netbase.pri)

SOURCES += \
        $$PWD/src/common/taskentry.cpp \
        $$PWD/src/common/taskqueue.cpp \
        $$PWD/src/net/netreading.cpp \
        $$PWD/src/net/netwriting.cpp \
        $$PWD/src/net/tcpclient.cpp \
        $$PWD/src/net/tcpserver.cpp \
        $$PWD/src/net/tcpsession.cpp \
        $$PWD/src/net/udpchannel.cpp


unix {
    target.path = /usr/lib
    INSTALLS += target
}
