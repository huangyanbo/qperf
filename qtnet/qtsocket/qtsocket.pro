#-------------------------------------------------
#
# Project created by QtCreator 2020-12-16T16:14:38
#
#-------------------------------------------------

QT       -= gui

TEMPLATE = lib

CONFIG(release, debug|release){
MOC_DIR = tmp/moc
RCC_DIR = tmp/rcc
OBJECTS_DIR = tmp/obj

TARGET = qtsocket
}
else{
MOC_DIR = tmpD/moc
RCC_DIR = tmpD/rcc
OBJECTS_DIR = tmpD/obj
TARGET = qtsocketd
}

DEFINES += QNET_LIBRARY
CONFIG += staticlib

# The following define makes your compiler emit warnings if you use
# any feature of Qt which has been marked as deprecated (the exact warnings
# depend on your compiler). Please consult the documentation of the
# deprecated API in order to know how to port your code away from it.
DEFINES += QT_DEPRECATED_WARNINGS

# You can also make your code fail to compile if you use deprecated APIs.
# In order to do so, uncomment the following line.
# You can also select to disable deprecated APIs only up to a certain version of Qt.
#DEFINES += QT_DISABLE_DEPRECATED_BEFORE=0x060000    # disables all the APIs deprecated before Qt 6.0.0

include($$PWD/../include/netbase.pri)

SOURCES += \
        ../src/common/taskentry.cpp \
        ../src/common/taskqueue.cpp \
        ../src/net/netreading.cpp \
        ../src/net/netwriting.cpp \
        ../src/net/tcpclient.cpp \
        ../src/net/tcpserver.cpp \
        ../src/net/tcpsession.cpp \
        ../src/net/udpchannel.cpp

HEADERS +=

unix {
    target.path = /usr/lib
    INSTALLS += target
}
