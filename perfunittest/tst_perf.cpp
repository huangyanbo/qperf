﻿#include "commonpacktest.h"
#include "providetest.h"
#include "readyargstest.h"
#include "replypackettest.h"
#include "requestpackettest.h"
#include "startargstest.h"
#include "stopargstest.h"

#include <QtTest>

#include <data/requestpacket.h>

// 对netPerf业务代码的测试类。
class perf : public QObject
{
    Q_OBJECT
public:
    perf();
    ~perf();

private slots:
    void initTestCase();
    void cleanupTestCase();
    void test_commonPack(); // 通用指令包封装测试。
    void test_commonPack_data(); // 通用指令包封装测试（数据）。
    void test_startCmd(); // 启动指令封装测试。
    void test_startCmd_data(); // 启动指令测试数据。。
    void test_stopCmd(); // 停止指令封装测试。
    void test_stopCmd_data(); // 停止指令测试数据。
    void test_requestPack(); //  请求数据包测试。
    void test_requestPack_data(); // 请求数据包测试数据。
    void test_replyPack(); // 响应数据包测试。
    void test_replyPack_data(); // 请求响应数据包测试数据。
    void test_readyArgs(); // 准备完毕数据包测试。
    void test_readyArgs_data(); // 准备完毕数据包测试数据。
    void test_qlist(); // 测试qlist。
private:
    ReplyPacketTest m_replyTest; // 测试响应数据包。
    RequestPacketTest m_requestTest; // 测试请求数据包。
    StopArgsTest m_stopTest; // 停止指令测试。
    StartArgsTest m_startTest;  // 启动指令测试对象。
    CommonPackTest m_commonPackTest; // 通用数据包的测试对象。
    ReadyArgsTest m_readyTest; //  准备完毕参数测试对象。
};

perf::perf()
{

}

perf::~perf()
{

}

void perf::initTestCase()
{

}

void perf::cleanupTestCase()
{

}

void perf::test_commonPack()
{
    m_commonPackTest.run();
}

void perf::test_commonPack_data()
{
    m_commonPackTest.run_data(); // 准备数据。
}

void perf::test_startCmd()
{
    m_startTest.run();
}

void perf::test_startCmd_data()
{
    m_startTest.run_data();
}

void perf::test_stopCmd()
{
    m_stopTest.run();
}

void perf::test_stopCmd_data()
{
    m_stopTest.run_data();
}

void perf::test_requestPack()
{
    m_requestTest.run();
}

void perf::test_requestPack_data()
{
    m_requestTest.run_data();
}

void perf::test_replyPack()
{
    m_replyTest.run();
}

void perf::test_replyPack_data()
{
    m_replyTest.run_data();
}

void perf::test_readyArgs()
{
    m_readyTest.run();
}

void perf::test_readyArgs_data()
{
    m_readyTest.run_data();
}

void perf::test_qlist()
{
    ProvideTest test;
    test.run();
}

QTEST_APPLESS_MAIN(perf)

#include "tst_perf.moc"
