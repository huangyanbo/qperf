﻿#ifndef COMMONPACKTEST_H
#define COMMONPACKTEST_H

class Data;
class CommonPack;
/**
 * @brief The CommonPackTest class
 * 通用数据包封包测试类。
 */
class CommonPackTest
{
public:
    CommonPackTest();
    /**
     * @brief run 执行测试方法。
     */
    void run();
    /**
     * @brief run_data
     * 构造测试所需的数据。
     */
    void run_data();
private:
    /**
     * @brief testCommonParser 测试通用数据解析。
     * @param src
     * @param expect
     */
    void testCommonParser(Data &src, CommonPack &expect);
    /**
     * @brief testCommonPackWriter 测试通用数据封装。
     * @param src
     * @param expectSize
     * @param dest
     */
    void testCommonPackWriter(CommonPack &src, int expectSize, Data &dest);
};

#endif // COMMONPACKTEST_H
