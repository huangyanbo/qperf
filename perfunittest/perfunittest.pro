QT += testlib
QT -= gui

CONFIG += qt console warn_on depend_includepath testcase
CONFIG -= app_bundle
CONFIG += c++11

TEMPLATE = app
INCLUDEPATH += $$PWD/../netperf
DEPENDPATH +=  $$PWD/../netperf
include($$PWD/../netperf/netperf.pri)

SOURCES +=  tst_perf.cpp \
    commonpacktest.cpp \
    providetest.cpp \
    readyargstest.cpp \
    replypackettest.cpp \
    requestpackettest.cpp \
    startargstest.cpp \
    stopargstest.cpp

HEADERS += \
    commonpacktest.h \
    providetest.h \
    readyargstest.h \
    replypackettest.h \
    requestpackettest.h \
    startargstest.h \
    stopargstest.h

