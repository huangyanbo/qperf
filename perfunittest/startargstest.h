﻿#ifndef STARTARGSTEST_H
#define STARTARGSTEST_H

/**
 * @brief The StartArgsTest class
 * 测试启动数据包的封装。
 */
class StartArgsTest
{
public:
    StartArgsTest();
    /**
     * @brief run_data 测试数据。
     */
    void run_data();
    /**
     * @brief run 启动测试。
     */
    void run();
};

#endif // STARTARGSTEST_H
