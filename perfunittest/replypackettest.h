﻿#ifndef REPLYPACKETTEST_H
#define REPLYPACKETTEST_H 
/**
 * @brief 表示响应数据包封装测试类。
 */
class ReplyPacketTest
{
public:
    ReplyPacketTest();
    virtual ~ReplyPacketTest();
    /**
    * @biref 执行准备数据。
    */
    void run_data();
    /**
    * @biref 执行测试。
    */
    void run();
}; 
#endif // REPLYPACKETTEST_H
