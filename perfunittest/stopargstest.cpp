﻿#include "stopargstest.h"

#include <data/stopargs.h>

#include <serialize/stopargsparser.h>
#include <serialize/stopargswriter.h>
#include <QtTest>

#include <QMetaType>

Q_DECLARE_METATYPE(Data)
Q_DECLARE_METATYPE(StopArgs)

StopArgsTest::StopArgsTest()
{
}

void StopArgsTest::run_data()
{
    QTest::addColumn<StopArgs>("testData");
    //  构造数据。
    StopArgs testData;
    PerfResult resultTest(10000, 1000, 0.123f, 561.01f, 0.001f);
    testData.setResult(resultTest);
    QTest::newRow("R1") << testData;


    PerfResult resultTest2(999, 112, 4.2f, 0.022f, 0.001f);
    testData.setResult(resultTest2);
    QTest::newRow("R2") << testData;
}

void StopArgsTest::run()
{
    QFETCH(StopArgs, testData);

    Data writerBuffer;
    StopArgsWriter writer;
    // 编码测试 。
    int writeSize = writer.retrieveSize(testData);
    bool bWrited = writer.write(testData, writerBuffer);

    QCOMPARE(writeSize, 24);
    QVERIFY(bWrited);

    QCOMPARE(writerBuffer.size(), writeSize); //   固定长度
    QCOMPARE(writerBuffer.offset(), 0);

    // 解析测试。
    StopArgsParser parser;
    StopArgs argsActual;
    int parseSize = parser.parse(writerBuffer, argsActual);
    QCOMPARE(parseSize, writeSize);

    // 解析结果测试。
    PerfResult actualRst = argsActual.result();
    PerfResult expectRst = testData.result();
    QCOMPARE(actualRst.testCount(), expectRst.testCount());
    QVERIFY2(abs(actualRst.minElapsed() - expectRst.minElapsed()) < 1e-10f,
            argsActual.toString().toStdString().data());
    QVERIFY2(abs(actualRst.maxElapsed() - expectRst.maxElapsed()) < 1e-10f,
             argsActual.toString().toStdString().data());
    QVERIFY2(abs(actualRst.avergeElapsed() - expectRst.avergeElapsed()) < 1e-10f,
            argsActual.toString().toStdString().data());
    QCOMPARE(actualRst.timeOutCount(), expectRst.timeOutCount());
}
