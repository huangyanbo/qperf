﻿#ifndef REQUESTPACKETTEST_H
#define REQUESTPACKETTEST_H

/**
 * @brief The RequestPacketTest class
 *  请求数据包封包的单元测试。
 */
class RequestPacketTest
{
public:
    RequestPacketTest();

    /**
      * @brief run_data
      */
    void run_data();
    /**
     * @brief run
     */
    void run();
};

#endif // REQUESTPACKETTEST_H
