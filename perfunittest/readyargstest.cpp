﻿#include "readyargstest.h"
#include <data/readyargs.h>
#include <serialize/readyargswriter.h>
#include <serialize/readyargsparser.h> 
#include <QTest>
#include <QMetaType>
Q_DECLARE_METATYPE(ReadyArgs);
Q_DECLARE_METATYPE(Data);
ReadyArgsTest::ReadyArgsTest()
{

}

ReadyArgsTest::~ReadyArgsTest()
{

}

void ReadyArgsTest::run_data()
{ 
    QTest::addColumn<ReadyArgs>("testData");
    QHostAddress address("192.168.0.1");
    quint16 port = 1234;
    ReadyArgs testData(address, port);
    //  待测试的数据赋值。
    QTest::addRow("R1") << testData ; 
    
    // 待测试的数据赋值。
    testData.setLocalPort(65535);
    QTest::addRow("R2") << testData ; 
    
    // 待测试的数据赋值。
    testData.setLocalPort(1);
    QTest::addRow("R3") << testData ;  
    
}

void ReadyArgsTest::run()
{
    //   取出构造的数据 开始测试 。
    QFETCH(ReadyArgs, testData);
   
   // 测试编码器。
    Data data;
    ReadyArgsWriter writer;
    int writeSize = writer.retrieveSize(testData);
    bool bWrited = writer.write(testData, data); 
    QVERIFY(writeSize > 0);
    QVERIFY(bWrited);
    QCOMPARE(data.remainSize(), writeSize);
    
    // 测试解析器。
    ReadyArgs destData;
    ReadyArgsParser parser;
    int parseSize = parser.parse(data, destData);
    QCOMPARE(parseSize, writeSize);
    
    // 验证destData与testData的值是否一致。
    QCOMPARE(destData.localHost(), testData.localHost());
    QCOMPARE(destData.localPort(), testData.localPort());
}
