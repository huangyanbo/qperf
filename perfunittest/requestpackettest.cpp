﻿#include "requestpackettest.h"
#include <QtTest>
#include <data/requestpacket.h>
#include <serialize/requestpacketparser.h>
#include <serialize/requestpacketwriter.h>
Q_DECLARE_METATYPE(RequestPacket)

RequestPacketTest::RequestPacketTest()
{

}

void RequestPacketTest::run_data()
{
    QTest::addColumn<RequestPacket>("testData");

    //  构造数据。
    RequestPacket argsTest;
    argsTest.setFlag(0x888888);
    QByteArray test(1000, char('\x99'));
    argsTest.setContent(test);
    QTest::newRow("Row-1") << argsTest;
}

void RequestPacketTest::run()
{
    QFETCH(RequestPacket, testData);

    // 编码测试。
    Data writerBuffer;
    RequestPacketWriter writer;
    int writeSize = writer.retrieveSize(testData);
    bool bWrited = writer.write(testData, writerBuffer);
    QVERIFY(writeSize > 0);
    QVERIFY(bWrited);
    QCOMPARE(writerBuffer.size(), testData.content().size() + 8); // 固定长度。
    QCOMPARE(writerBuffer.offset(), 0);

    // 解析测试。
    RequestPacketParser parser;
    RequestPacket argsActual;
    int parseSize = parser.parse(writerBuffer, argsActual);
    QCOMPARE(parseSize, writeSize);
    QCOMPARE(argsActual.flag(), testData.flag());
    QCOMPARE(argsActual.content(), testData.content()); //  确定是否可行。

}
