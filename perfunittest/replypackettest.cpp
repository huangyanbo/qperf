﻿#include "replypackettest.h"
#include <data/replypacket.h>
#include <serialize/replypacketwriter.h>
#include <serialize/replypacketparser.h> 
#include <QTest>
#include <QMetaType>
Q_DECLARE_METATYPE(ReplyPacket);
ReplyPacketTest::ReplyPacketTest()
{

}

ReplyPacketTest::~ReplyPacketTest()
{

}

void ReplyPacketTest::run_data()
{ 
    QTest::addColumn<ReplyPacket>("testData"); 
    ReplyPacket testData;
    QByteArray testBuffer(100, char(0x11));
    testData.setFlag(1999);
    testData.setContent(testBuffer);
    //  待测试的数据赋值。
    QTest::addRow("R1") << testData ;
    
}

void ReplyPacketTest::run()
{
    //   取出构造的数据 开始测试 。
    QFETCH(ReplyPacket, testData);
   
   // 测试编码器。
    Data data;
    ReplyPacketWriter writer;
    int writeSize = writer.retrieveSize(testData);
    bool bWrited = writer.write(testData, data); 
    QVERIFY(writeSize > 0);
    QVERIFY(bWrited);
    QCOMPARE(data.remainSize(), writeSize);
    
    // 测试解析器。
    ReplyPacket destData;
    ReplyPacketParser parser;
    int parseSize = parser.parse(data, destData);
    QCOMPARE(parseSize, writeSize);
    
    // 验证destData与testData的值是否一致。
    QCOMPARE(destData.flag(), testData.flag());
    QCOMPARE(destData.content(), testData.content());
}
