﻿#include "startargstest.h"

#include <serialize/startargsparser.h>
#include <serialize/startargswriter.h>
#include <data/startargs.h>
#include <QtTest>

#include <QMetaType>

Q_DECLARE_METATYPE(StartArgs)

StartArgsTest::StartArgsTest()
{
}

void StartArgsTest::run_data()
{
    QTest::addColumn<StartArgs>("testData");

    IPFiveTuple tupleTest;
    tupleTest.setType(IPFiveTuple::Type_TcpClient);
    tupleTest.setLocalAddr(QHostAddress("127.0.0.1"));
    tupleTest.setLocalPort(9898);
    tupleTest.setRemoteAddr(QHostAddress("0.0.0.0"));
    tupleTest.setRemotePort(6666);

    //  构造数据。
    StartArgs argsTest;
    argsTest.setTimeOut(1000);
    argsTest.setPackFreq(6500);
    argsTest.setTotalCount(10202);
    argsTest.setPerPackSize(999);
    argsTest.setIpTuple(tupleTest);
    argsTest.setWarmPack(0);
    QTest::newRow("Row-TcpClient") << argsTest;

    // 重新构造数据2。
    tupleTest.setType(IPFiveTuple::Type_Udp);
    argsTest.setTimeOut(10000);
    argsTest.setPackFreq(100000);
    argsTest.setTotalCount(1000000000);
    argsTest.setIpTuple(tupleTest);
    argsTest.setWarmPack(65535);
    QTest::newRow("Row-Udp") << argsTest;

    // 重新构造数据3。
    tupleTest.setType(IPFiveTuple::Type_TcpServer);
//    tupleTest.setLocalAddr(QHostAddress(QHostAddress::Any));  TODO Any的是否无法回转，是否有问题？
    argsTest.setTimeOut(1000000);
    argsTest.setPackFreq(1);
    argsTest.setTotalCount(100);
    argsTest.setIpTuple(tupleTest);
    argsTest.setWarmPack(1);
    QTest::newRow("Row—TcpServer") << argsTest ;
}

void StartArgsTest::run()
{
    QFETCH(StartArgs, testData);

    // 编码测试。
    Data writerBuffer;
    StartArgsWriter writer;
    int writeSize = writer.retrieveSize(testData);
    bool bWrited = writer.write(testData, writerBuffer);
    QVERIFY(writeSize > 0);
    QVERIFY(bWrited);
    QCOMPARE(writerBuffer.size(), 32); // 固定长度。
    QCOMPARE(writerBuffer.offset(), 0);

    // 解析测试。
    StartArgsParser parser;
    StartArgs argsActual;
    int parseSize = parser.parse(writerBuffer, argsActual);
    QCOMPARE(parseSize, writeSize);

    QCOMPARE(argsActual.timeOut(), testData.timeOut());
    QCOMPARE(argsActual.packFreq(), testData.packFreq());
    QCOMPARE(argsActual.totalCount(), testData.totalCount());
    QCOMPARE(argsActual.perPackSize(), testData.perPackSize());
    QCOMPARE(argsActual.warmPack(), testData.warmPack());

    IPFiveTuple tupleTest= testData.ipTuple();
    IPFiveTuple actualTuple = argsActual.ipTuple();
    QCOMPARE(actualTuple.type(), tupleTest.type());
    QCOMPARE(actualTuple.remoteAddr(), tupleTest.remoteAddr());
    QCOMPARE(actualTuple.remotePort(), tupleTest.remotePort());
    QCOMPARE(actualTuple.localAddr(), tupleTest.localAddr());
    QCOMPARE(actualTuple.localPort(), tupleTest.localPort());
}
