﻿#ifndef INITARGS_H
#define INITARGS_H

#include <ipfivetuple.h>


/**
 * @brief The InitArgs class
 *  初始参数。
 */
class InitArgs
{
public:
    InitArgs();
    InitArgs(const IPFiveTuple &tuple);

    IPFiveTuple tuple() const;
    void setTuple(const IPFiveTuple &tuple);

private:
    IPFiveTuple m_tuple;
};

#endif // INITARGS_H
