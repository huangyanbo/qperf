﻿#include "readyargs.h" 
 
ReadyArgs::ReadyArgs()
{

}

ReadyArgs::ReadyArgs(const QHostAddress &localHost, quint16 localPort)
{
    m_localHost = localHost;
    m_localPort = localPort;
}

ReadyArgs::~ReadyArgs()
{

}

QHostAddress ReadyArgs::localHost() const
{
    return m_localHost;
}

void ReadyArgs::setLocalHost(const QHostAddress &localHost)
{
    m_localHost = localHost;
}

quint16 ReadyArgs::localPort() const
{
    return m_localPort;
}

void ReadyArgs::setLocalPort(const quint16 &localPort)
{
    m_localPort = localPort;
}

