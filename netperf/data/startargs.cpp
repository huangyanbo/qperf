﻿#include "startargs.h"

StartArgs::StartArgs()
{
    m_perPackSize = 1000; // 每包字节长度。
    m_totalCount = 100; // 总数据包长度。
    m_packFreq = 10; // 测试频率（包/s）。
    m_timeOut = 1500 ; // 超时长度ms。
    m_warmPack = 0; // 热身数据包，默认0.
}

int StartArgs::packFreq() const
{
    return m_packFreq;
}

void StartArgs::setPackFreq(int packFreq)
{
    m_packFreq = packFreq;
}

int StartArgs::totalCount() const
{
    return m_totalCount;
}

void StartArgs::setTotalCount(int totalSize)
{
    m_totalCount = totalSize;
}

int StartArgs::perPackSize() const
{
    return m_perPackSize;
}

void StartArgs::setPerPackSize(int perPackSize)
{
    m_perPackSize = perPackSize;
}

int StartArgs::timeOut() const
{
    return m_timeOut;
}

void StartArgs::setTimeOut(int timeOut)
{
    m_timeOut = timeOut;
}

IPFiveTuple StartArgs::ipTuple() const
{
    return m_ipTuple;
}

void StartArgs::setIpTuple(const IPFiveTuple &ipTuple)
{
    m_ipTuple = ipTuple;
}

int StartArgs::warmPack() const
{
    return m_warmPack;
}

void StartArgs::setWarmPack(int warmPack)
{
    m_warmPack = warmPack;
}

QString StartArgs::toString()
{
    QString info;
    QTextStream stream(&info);
    stream << QObject::tr("Perpacket size:")
           << perPackSize()
           << QObject::tr(" Total count:")
           << totalCount()
           << QObject::tr("  Packet Frequency:")
           << packFreq()
           << QObject::tr(" (")
           << QObject::tr(" expect bandwith(M/s):")
           << packFreq() * (perPackSize() + 16) * 1e-6
           << QObject::tr(" )")
           << QObject::tr("  Timeout(ms):")
           << timeOut()
           << QObject::tr(" Warm packet count:")
           << warmPack()
           << QObject::tr(" IP five tuple:")
           << m_ipTuple.toString();
    return info;
}
