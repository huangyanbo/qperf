﻿#include "stopargs.h"

#include <QTextStream>

StopArgs::StopArgs()
{

}

PerfResult StopArgs::result() const
{
    return m_result;
}

void StopArgs::setResult(const PerfResult &result)
{
    m_result = result;
}

QString StopArgs::toString() const
{
    QString tmp;
    QTextStream stream(&tmp);
    stream << QObject::tr("TestCount:") << m_result.testCount()
          << QObject::tr("; TimeOutCount:") << m_result.timeOutCount()
          << QObject::tr("; AvergeElapsed:") << m_result.avergeElapsed()
          << QObject::tr("; MaxElapsed:") << m_result.maxElapsed()
          << QObject::tr("; MinElapsed:") << m_result.minElapsed();
    return tmp;
}
