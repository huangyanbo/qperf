﻿#include "initargs.h"

InitArgs::InitArgs()
{

}

InitArgs::InitArgs(const IPFiveTuple &tuple)
{
    m_tuple = tuple;
}

IPFiveTuple InitArgs::tuple() const
{
    return m_tuple;
}

void InitArgs::setTuple(const IPFiveTuple &tuple)
{
    m_tuple = tuple;
}
