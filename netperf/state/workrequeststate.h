﻿#ifndef WORKREQUESTSTATE_H
#define WORKREQUESTSTATE_H

#include "basestate.h"

#include <QVector>
#include <pointer/pointercallback.h>
class QThread;
class QTime;
class RateControl;
/**
 * @brief The WorkRequestState class
 *  表示请求工作状态。
 */
class WorkRequestState : public BaseState,
        public PointerCallback
{
public:
    WorkRequestState(Context *context, bool bServer);
    ~WorkRequestState() override;
    // PerfState interface
public:
    void process() override;
    void cancel() override;

    // PointerCallback interface
public:
    void onStateChanged(PointerCallback::State state, bool bCmd) override;
    void onStart(const StartArgs &args) override;
    void onStop(const StopArgs &args) override;
    void onRequest(const RequestPacket &pack) override;
    void onReply(const ReplyPacket &pack) override;
    void onReady(const ReadyArgs &args) override;
protected:
    /**
      * @brief runWorker 执行工作的函数。
      */
     void runWorker();
private:
     /**
      * @brief resultCompute 结果计算。
      * @param timeOut 超时时间（毫秒）。
      * @param warmCount 热身包数。
      * @param args 输出结果参数。
      */
     void resultCompute(int timeOut, int warmCount, StopArgs &args);
     /**
      * @brief runRequest 执行请求。
      * @param startArgs 启动参数。
      */
     void runRequest(StartArgs *startArgs);
     /**
      * @brief request 单个请求。
      * @param rateCtrl 速率控制。
      * @param pack 数据包。
      * @return 请求失败返回false。
      */
     bool request(RateControl &rateCtrl, RequestPacket &pack);

     void workComplete(StartArgs *startArgs);
private:
     /**
      * @brief The Record class
      * 请求记录类。表示一条请求记录。
      */
     class  Record
     {
     public:
          Record(int flags);
         ~Record();
         /**
          * @brief start 开始记录。
          */
         void start();
         /**
          * @brief stop 停止记录。
          */
         void stop();
         /**
          * @brief elapsed 记录的时间。
          * @return
          */
         float elapsed();
     private:
         int m_flags; // 请求的参数。
         QTime *m_time; // 时间。
         float m_elaped;
         bool m_stopped; // 停止。
     };
private:
    QVector<Record *> m_records; // 记录的内部集合。
    QThread *m_workThread; // 工作线程。
};

#endif // WORKINGSTATE_H
