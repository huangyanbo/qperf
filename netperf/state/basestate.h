﻿#ifndef BASESTATE_H
#define BASESTATE_H

#include <QString>


class Context;
/**
 * @brief The BaseState class
 *   性能状态基类。
 */
class BaseState
{
public:
    enum StateType{
        Final = 0, // 终止状态。
        InitServer = 1, // 服务端初始状态。
        InitClient = 2, // 客户端初始状态。
        StartServer = 3, //  服务端启动状态。
        StartClient = 4,  // 客户端启动状态。
        WorkRequest = 5, //  请求状态。
        WorkReply = 6, // 响应状态。
        Stop = 7, // 停止状态。
    };
public:
    /**
     * @brief PerfState 状态构造。
     * @param context 上下文。
     * @param server  服务端标识。
     */
    BaseState(Context *context, bool server);
    virtual ~BaseState();
    /**
     * @brief process 完成。
     */
    virtual void process() = 0;
    /**
     * @brief cancel 取消。
     */
    virtual void cancel() = 0;
    /**
     * @brief type 状态。
     * @return
     */
    BaseState::StateType type() const;
    /**
     * @brief change 创建并改变状态。
     * @param type
     * @return
     */
     void change(BaseState::StateType type);
     /**
      * @brief stateName 获取类型的字符名称。
      * @return 返回当前类型的字符串。
      */
     QString stateName() const;
private:
    /**
     * @brief setType 设置状态。
     * @param type 状态。
     */
    void setType(const BaseState::StateType &type);

protected:
    Context *m_context;
    StateType m_type;
    bool m_bServer; // 服务端标识。
    friend class StateFactory;
};

#endif // BASESTATE_H
