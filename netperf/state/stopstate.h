﻿#ifndef STOPSTATE_H
#define STOPSTATE_H

#include "basestate.h"


/**
 * @brief The StoppingState class
 *  表示停止状态。
 */
class StopState : public BaseState
{
public:
    StopState(Context *context, bool bServer);

    // PerfState interface
public:
    void process() override;
    void cancel() override;
};

#endif // STOPPINGSTATE_H
