﻿#include "basestate.h"
#include "statefactory.h"
#include <context/context.h>

BaseState::BaseState(Context *context, bool bServer)
{
    m_context = context;
    m_bServer = bServer;
    m_type = Final;
}

BaseState::~BaseState()
{

}

BaseState::StateType BaseState::type() const
{
    return m_type;
}

void BaseState::change(BaseState::StateType type)
{
    StateFactory factory(type, m_bServer);
    BaseState *state = factory.create(m_context);
    m_context->changeState(state);
}

QString BaseState::stateName() const
{
    switch(m_type)
    {
    case BaseState::Final: return QObject::tr("final state");
    case BaseState::InitServer: return QObject::tr("init server state");
    case BaseState::InitClient: return QObject::tr("init client state");
    case BaseState::StartServer: return QObject::tr("start server state");
    case BaseState::StartClient: return QObject::tr("start client state");
    case BaseState::WorkRequest: return QObject::tr("request state");
    case BaseState::WorkReply: return QObject::tr("reply state");
    case BaseState::Stop: return QObject::tr("stop state");
    }
    return "Unexpect state";
}

void BaseState::setType(const BaseState::StateType &type)
{
    m_type = type;
}
