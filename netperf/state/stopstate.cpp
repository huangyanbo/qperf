﻿#include "stopstate.h"
#include <context/context.h>
#include "initserverstate.h"
#include <pointer/basepointer.h>

StopState::StopState(Context *context, bool bServer)
    :BaseState (context, bServer)
{
}

void StopState::process()
{
    //  停止的业务..需要区别服务端和客户端。
    if(m_bServer) // 服务端。重置状态为Init
    {
        m_context->disConnect(true);
        m_context->close(false);
        change(BaseState::InitServer);
    }else { // 客户端。重置状态为未初始。
        m_context->close(true);
        m_context->close(false);
        change(BaseState::Final);
    }
}

void StopState::cancel()
{
    process();
}
