﻿#include "initclientstate.h"
#include "initserverstate.h"
#include "startclientstate.h"
#include "startserverstate.h"
#include "statefactory.h"
#include "stopstate.h"
#include "finalstate.h"
#include "workreplystate.h"
#include "workrequeststate.h"

#include <context/context.h>

StateFactory::StateFactory(BaseState::StateType type, bool bServer)
{
    m_type = type;
    m_bServer = bServer;
}

BaseState *StateFactory::create(Context *context)
{
    BaseState * state = nullptr;
    switch (m_type) {
    case BaseState::Final:
        state = new FinalState(context, m_bServer);
        break;
    case BaseState::InitServer:
        state = new InitServerState(context, m_bServer);
        break;
    case BaseState::InitClient:
        state = new InitClientState(context, m_bServer);
        break;
    case BaseState::StartServer:
        state = new StartServerState(context, m_bServer);
        break;
    case BaseState::StartClient:
        state = new StartClientState(context, m_bServer);
        break;
    case BaseState::WorkRequest:
        state = new WorkRequestState(context, m_bServer);
        break;
    case BaseState::WorkReply:
        state = new WorkReplyState(context, m_bServer);
        break;
    case BaseState::Stop:
        state = new StopState(context, m_bServer);
        break;
    }
    if(state)
    {
        state->setType(m_type);
    }
    return state;
}
