﻿#ifndef INITSERVERSTATE_H
#define INITSERVERSTATE_H

#include "basestate.h"

#include <pointer/pointercallback.h>

class Context;
/**
 * @brief The InitServerState class
 * 表示 初始服务端状态。
 */
class InitServerState : public BaseState,
        public PointerCallback
{
public:
    InitServerState(Context *context, bool bServer);
    ~InitServerState() override;
    // PerfState interface
public:
    void process() override;
    void cancel() override;

    // PointerCallback interface
public:
    void onStateChanged(PointerCallback::State state, bool bCmd) override;
    void onStart(const StartArgs &args) override;
    void onStop(const StopArgs &args) override;
    void onRequest(const RequestPacket &pack) override;
    void onReply(const ReplyPacket &pack) override;
    void onReady(const ReadyArgs &args) override;
};

#endif // INITSTATE_H
