﻿#ifndef STARTCLIENTSTATE_H
#define STARTCLIENTSTATE_H

#include "basestate.h"
#include <pointer/pointercallback.h>
#include <data/readyargs.h>
/**
 * @brief The StartClientState class
 *    表示 客户端的初始状态。
 */
class StartClientState : public BaseState,
        public PointerCallback
{
public:
    StartClientState(Context *context, bool bServer);
    ~StartClientState() override;
    // BaseState interface
public:
    void process() override;
    void cancel() override;

    // PointerCallback interface
public:
    void onStateChanged(PointerCallback::State state, bool bCmd) override;
    void onStart(const StartArgs &args) override;
    void onStop(const StopArgs &args) override;
    void onRequest(const RequestPacket &pack) override;
    void onReply(const ReplyPacket &pack) override;
    void onReady(const ReadyArgs &args) override;
private:
    /**
     * @brief changeNext 改变下一状态。
     */
    void changeNext();
private:
    QAtomicInt m_startCounter; // 启动记录类。
    ReadyArgs m_readyArgs; // 准备就绪参数。
};

#endif // STARTCLIENTSTATE_H
