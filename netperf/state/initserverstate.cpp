﻿#include "initserverstate.h"
#include "startserverstate.h"
#include "stopstate.h"

#include <pointer/basepointer.h>

#include <tunnel/basetunnel.h>
#include <context/context.h>
#include <QDebug>
#include <ipfivetuple.h>

InitServerState::InitServerState(Context *context, bool bServer)
    :BaseState(context, bServer)
{
}

InitServerState::~InitServerState()
{
    m_context->remove(this);
}

void InitServerState::process()
{
    m_context->add(this);
}

void InitServerState::cancel()
{
    change(BaseState::Final);
}

void InitServerState::onStateChanged(PointerCallback::State state, bool bCmd)
{
    if(bCmd && state == PointerCallback::Connected)
    {
        qDebug() << QObject::tr("Change to StartServer State.");
        change(BaseState::StartServer); // 切换状态为服务端start
    }
}

void InitServerState::onStart(const StartArgs &args)
{
    Q_UNUSED(args)
}

void InitServerState::onStop(const StopArgs &args)
{
    Q_UNUSED(args)
}

void InitServerState::onRequest(const RequestPacket &pack)
{
    Q_UNUSED(pack)
}

void InitServerState::onReply(const ReplyPacket &pack)
{
    Q_UNUSED(pack)
}

void InitServerState::onReady(const ReadyArgs &args)
{
    Q_UNUSED(args)
}
