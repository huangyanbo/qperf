﻿#ifndef CLIENTPROCESS_H
#define CLIENTPROCESS_H

#include <pointer/pointercallback.h>

#include <QVector>
class Context;
class StartArgs;
class StopArgs;
class RequestPacket;
class ReplyPacket;
class ReadyArgs;
class Record;
class QThread;
class RateControl;

/**
 * @brief The ClientProcess class 表示客户端处理业务逻辑。
 */
class ClientProcess :
        public PointerCallback
{
public:
    ClientProcess(Context *context);
    ~ClientProcess() override;

    // PointerCallback interface
public:
    void onStateChanged(PointerCallback::State state, bool bCmd) override;
    void onStart(const StartArgs &args) override;
    void onStop(const StopArgs &args) override;
    void onRequest(const RequestPacket &pack) override;
    void onReply(const ReplyPacket &pack) override;
    void onReady(const ReadyArgs &args) override;
private:
    void startRequest();
    void runWorker();
    void runRequest(const StartArgs *startArgs);
    void workComplete(const StartArgs *startArgs);

    void resultCompute(int timeOut, int warmCount, StopArgs &args);

    bool request(RateControl &rateCtrl, RequestPacket &pack);
private:
    Context *m_context;
    QAtomicInt m_startCounter; // 启动记录类。
    QVector<Record *> m_records; // 记录的内部集合。
    QThread *m_workThread; // 工作线程。
};

#endif // CLIENTPROCESS_H
