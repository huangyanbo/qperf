﻿#ifndef STATEFACTORY_H
#define STATEFACTORY_H

#include "basestate.h"


/**
 * @brief The StateFactory class 状态工厂。
 */
class StateFactory
{
public:
    /**
     * @brief StateFactory
     * @param type
     * @param bServer  是否服务端。
     */
    StateFactory(BaseState::StateType type, bool bServer);
    /**
     * @brief create  创建状态实例。
     * @param context 上下文实例。
     * @return 返回状态实例。
     */
    BaseState *create(Context *context);
private:
    BaseState::StateType m_type;
    bool m_bServer;  // 是否服务端。
};

#endif // STATEFACTORY_H
