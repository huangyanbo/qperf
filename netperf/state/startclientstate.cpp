﻿#include "startclientstate.h"

#include <context/context.h>
#include <data/startargs.h>
#include <pointer/basepointer.h>

StartClientState::StartClientState(Context *context, bool bServer)
    : BaseState (context, bServer)
{
}

StartClientState::~StartClientState()
{
    m_context->remove(this);
}

void StartClientState::process()
{
    m_context->add(this);
    m_context->start(); // 使用内部初始的start参数。
}

void StartClientState::cancel()
{
    change(BaseState::Stop);
}

void StartClientState::onStateChanged(PointerCallback::State state, bool bCmd)
{
    if(state != PointerCallback::Connected)
    {
        cancel();
        return;
    }
    if(!bCmd) // 完成数据链路的链路建立。
    {
        changeNext();
    }
}

void StartClientState::onStart(const StartArgs &args)
{
    Q_UNUSED(args)
}

void StartClientState::onStop(const StopArgs &args)
{
    Q_UNUSED(args)
}

void StartClientState::onRequest(const RequestPacket &pack)
{
    Q_UNUSED(pack)
}

void StartClientState::onReply(const ReplyPacket &pack)
{
    Q_UNUSED(pack)
}

void StartClientState::onReady(const ReadyArgs &args)
{
    m_readyArgs = args; // TODO 收到的参数（IP、端口信息）最后置于数据链路中。
    changeNext();
}

void StartClientState::changeNext()
{
    m_startCounter.fetchAndAddAcquire(1);
    if(m_startCounter >= 2)
    {
        change(BaseState::WorkRequest);
    }
}
