﻿#ifndef INTEGRATIONTEST_H
#define INTEGRATIONTEST_H
class Context;
class BasePointer;
/**
 * @brief The IntegrationTest class
 * 业务集成测试类。
 */
class IntegrationTest
{
public:
    IntegrationTest();
    ~IntegrationTest();
    /**
     * @brief run 执行测试。
     * @param bServer 是否服务端的测试。
     */
    void run(bool bServer);
private:
    Context *m_context;
    BasePointer *m_pointer;
};

#endif // INTEGRATIONTEST_H
