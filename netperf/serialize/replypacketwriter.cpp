﻿#include "replypacketwriter.h"
#include <data/replypacket.h>
#include <QtEndian>
ReplyPacketWriter::ReplyPacketWriter()
{

}

ReplyPacketWriter::~ReplyPacketWriter()
{

}

int  ReplyPacketWriter::retrieveSize(const ReplyPacket &src)
{
    int varSize = 0; // 变长。
    varSize += src.content().size(); //ContentSize 的长度。
    int packSize = 8 + varSize;
    return packSize; // 返回实体编码后数据长度。
}

bool ReplyPacketWriter::write(const ReplyPacket &src, Data &dest)
{
    // 编码逻辑。  
    QByteArray buffer; // 定义写入的数据buffer。
    int offset = 0; // 定义写入数据的偏移。
    
    // 唯一标识字段的编码。 4 字节。
    int flag = src.flag(); // 直接关联字段赋值。
    flag = qToBigEndian<int>(flag);// 字节序重置。 
    buffer.append(reinterpret_cast<char *>(&flag), 4);
    offset += 4;
    
    // content().size()字段的编码。 4 字节。
    int contentSize = src.content().size();  // 间接关联字段赋值。
    contentSize = qToBigEndian<int>(contentSize);// 字节序重置。 
    buffer.append(reinterpret_cast<char *>(&contentSize), 4);
    offset += 4;
    
    // 数据内容字段的编码。 contentSize 字节。
    QByteArray content = src.content(); // 直接关联字段赋值。
    buffer.append(content);  
    offset += contentSize;
    
    dest.setBuffer(buffer);
    dest.setOffset(0);
    return true;
}