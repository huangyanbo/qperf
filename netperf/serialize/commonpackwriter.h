﻿#ifndef COMMONPACKWRITER_H
#define COMMONPACKWRITER_H

#include "idatawriter.h"
 
class CommonPack;
 
/**
 * @brief 表示通用数据封包编码器。
 * 数据包封装格式如下：(使用大端字节序）
 * ================================================
 * 字节序  字节长度    名称  描述
 * 0    4   协议头 固定0x12123434。
 * 4    2   数据类型    数据类型。由CommonPack::Type定义。
 * 6    4   数据长度    数据内容长度(contentSize) 取值[0, 10,000,000]。
 *10    6   保留字段    对齐使用。
 *16    变长  数据内容    长度为contentSize，内容由CommonPack::Type定义。
 * ================================================
 */
class CommonPackWriter : public IDataWriter<CommonPack>
{
public:
    CommonPackWriter();
    virtual ~CommonPackWriter() override; 
    // IDataWriter interface
public:
    int retrieveSize(const CommonPack &src) override;
    bool write(const CommonPack &src, Data &dest) override;
private:
    const int HeadFlg = 0x12123434; // 固定头标识
    const int MaxContentSize = 10000000; // 最大数据内容。
};

#endif // COMMONPACKWRITER_H
