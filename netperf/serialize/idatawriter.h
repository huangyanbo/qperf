﻿#include "data.h"

#ifndef IDATAWRITER_H
#define IDATAWRITER_H

/**
 * 表示数据编码接口。
 */
template<typename TData>
class IDataWriter
{

public:
    IDataWriter()
    {

    }

    virtual ~IDataWriter()
    {

    }

    /**
     * @brief retrieveSize 获取数据实体表示的字节长度。
     * @param src  数据实体。
     * @return  返回字节长度。
     */
    virtual int retrieveSize(const TData& src) = 0;

    /**
     * @brief write 数据实体写入到缓存。
     * @param src 数据实体源。
     * @param dest 目的缓存。
     * @return 成功返回true，否则返回false。
     */
    virtual bool write(const TData& src, Data& dest) = 0;

};

#endif // HEADER 
