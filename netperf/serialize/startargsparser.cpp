﻿#include "startargsparser.h" 
#include <data/startargs.h> 
#include <QtEndian>
 
StartArgsParser::StartArgsParser()
{

}

StartArgsParser::~StartArgsParser()
{

}

int StartArgsParser::parse(const Data &srcData, StartArgs &destData)
{
    // 解析逻辑。
    QByteArray srcArray = srcData.buffer();
    int index = srcData.offset();
    const char *srcBuf = srcArray.data();
    if(srcData.size() - index < 32)
    {
        return 0;
    }

    // 发送频率 4字节。
    int sendFreq = qFromBigEndian<int>(&srcBuf[index]);
    index += 4;
    destData.setPackFreq(sendFreq);

    // 总包数 4字节。
    int totalPack = qFromBigEndian<int>(&srcBuf[index]);
    index += 4;
    destData.setTotalCount(totalPack);

    // 每包数据长度 4字节。
    int perPackSize = qFromBigEndian<int>(&srcBuf[index]);
    index += 4;
    destData.setPerPackSize(perPackSize);

    // 超时时间 4字节。
    int timeOut = qFromBigEndian<int>(&srcBuf[index]);
    index += 4;
    destData.setTimeOut(timeOut);

    IPFiveTuple tuple;
    // 服务类型 1字节。
    quint8 type = quint8(srcBuf[index]);
    index ++;
    tuple.setType(IPFiveTuple::Type(type));

    // 源地址 4字节。
    quint32 srcAddr = qFromBigEndian<quint32>(&srcBuf[index]);
    index += 4;
    tuple.setLocalAddr(QHostAddress(srcAddr));

    // 目的地址 4字节。
    quint32 destAddr = qFromBigEndian<quint32>(&srcBuf[index]);
    index += 4;
    tuple.setRemoteAddr(QHostAddress(destAddr));

    // 源端口  2字节。3
    quint16 srcPort = qFromBigEndian<quint16>(&srcBuf[index]);
    index += 2;
    tuple.setLocalPort(srcPort);

    // 目的端口 2字节。
    quint16 destPort = qFromBigEndian<quint16>(&srcBuf[index]);
    index += 2;
    tuple.setRemotePort(destPort);

    // 热身数据包 2字节。
    quint16 warmSize = qFromBigEndian<quint16>(&srcBuf[index]);
    index += 2;
    destData.setWarmPack(warmSize);

    // 保留 1字节不解析。
    index += 1;

    destData.setIpTuple(tuple);
    return index;
}
