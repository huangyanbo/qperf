﻿#include "requestpacketparser.h" 
#include <data/requestpacket.h>
#include <QtEndian>
RequestPacketParser::RequestPacketParser()
{

}

RequestPacketParser::~RequestPacketParser()
{

}

int RequestPacketParser::parse(const Data &srcData, RequestPacket &destData)
{
    QByteArray srcBuf = srcData.buffer();
    const char *pSrc = srcBuf.data();
    int index = srcData.offset();
    
    // 唯一标识字段的解析。 4 字节。
    if(index + 4 > srcBuf.size()){
        return 0; // 返回0表示当前数据的长度不足。
    }
    int flag;
    flag = qFromBigEndian<int>(&pSrc[index]); // 解析字段。
    destData.setFlag(flag); // 赋值
    index += 4;   
    
    // content().size()字段的解析。 4 字节。
    if(index + 4 > srcBuf.size()){
        return 0; // 返回0表示当前数据的长度不足。
    }
    int contentSize;
    contentSize = qFromBigEndian<int>(&pSrc[index]); // 解析字段。
    index += 4;   
    
    // 数据内容字段的解析。 contentSize 字节。
    if(index + contentSize > srcBuf.size()){
        return 0; // 返回0表示当前数据的长度不足。
    }
    QByteArray content;
    content.append(&pSrc[index], contentSize); // 解析字段。
    destData.setContent(content); // 赋值
    index += contentSize;   
    
    return index;
}