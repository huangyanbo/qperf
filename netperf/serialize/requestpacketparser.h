﻿#ifndef REQUESTPACKETPARSER_H
#define REQUESTPACKETPARSER_H

#include "idataparser.h"
 
class RequestPacket;

/**
 * @brief 请求数据包实体。
 */
class RequestPacketParser : public IDataParser<RequestPacket>
{
public:
    RequestPacketParser();
    virtual ~RequestPacketParser() override;
    
    // IDataParser interface
public: 
    int parse(const Data &srcData, RequestPacket &destData) override;
};
 

#endif // REQUESTPACKETPARSER_H 
