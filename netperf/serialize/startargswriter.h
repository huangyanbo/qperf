﻿#ifndef STARTARGSWRITER_H
#define STARTARGSWRITER_H

#include "idatawriter.h"
 
class StartArgs;

/**
 * @brief 表示启动参数编码器。
 * 数据包封装格式如下：(使用大端字节序）
 * ================================================
 * 字节序  字节长度    名称  描述
 * 0    4   数据发包频率  单位：包/s，无符号整数
 * 4    4   总包数  测试发送总包数。无符号整数
 * 8    4   每包数据长度    无符号整数
 *12    4   超时时间    单位：ms.
 *16    1   服务类型   取值见：IPFiveTuple:Type。
 *17    4   源地址 IPv4地址。
 *21    4   目的地址    IPv4地址。
 *25    2   源端口
 *27    2   目的端口
 *29    3   保留数据
 * ================================================
 */
class StartArgsWriter : public IDataWriter<StartArgs>
{
public:
    StartArgsWriter();
    virtual ~StartArgsWriter() override; 
    // IDataWriter interface
public:
    int retrieveSize(const StartArgs &src) override;
    bool write(const StartArgs &src, Data &dest) override;
};

#endif // STARTARGSWRITER_H
