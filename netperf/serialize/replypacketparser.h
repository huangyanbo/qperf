﻿#ifndef REPLYPACKETPARSER_H
#define REPLYPACKETPARSER_H

#include "idataparser.h"
 
class ReplyPacket;

   

/**
 * @brief 回复数据包实体。
 */
class ReplyPacketParser : public IDataParser<ReplyPacket>
{
public:
    ReplyPacketParser();
    virtual ~ReplyPacketParser() override;
    
    // IDataParser interface
public: 
    int parse(const Data &srcData, ReplyPacket &destData) override;
};
 

#endif // REPLYPACKETPARSER_H 