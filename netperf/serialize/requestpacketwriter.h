﻿#ifndef REQUESTPACKETWRITER_H
#define REQUESTPACKETWRITER_H

#include "idatawriter.h"
 
class RequestPacket;

/**
 * @brief 表示请求数据包实体编码器。
 */
class RequestPacketWriter : public IDataWriter<RequestPacket>
{
public:
    RequestPacketWriter();
    virtual ~RequestPacketWriter() override; 
    // IDataWriter interface
public:
    int retrieveSize(const RequestPacket &src) override;
    bool write(const RequestPacket &src, Data &dest) override;
};

#endif // REQUESTPACKETWRITER_H
