﻿#ifndef READYARGSWRITER_H
#define READYARGSWRITER_H
#include "idatawriter.h"
class ReadyArgs;
/**
 * @brief 表示准备完毕参数编码器。
 */
class ReadyArgsWriter : public IDataWriter<ReadyArgs>
{
public:
    ReadyArgsWriter();
    virtual ~ReadyArgsWriter() override; 
    // IDataWriter interface
public:
    int retrieveSize(const ReadyArgs &src) override;
    bool write(const ReadyArgs &src, Data &dest) override;
};
#endif // READYARGSWRITER_H
