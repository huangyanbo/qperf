﻿#include "qconsoledest.h"
#include <QDebug>

QConsoleDest::QConsoleDest()
{

}

void QConsoleDest::write(const QString &message, Level level)
{
    Q_UNUSED(level)
    qWarning() << message.toStdString().data();
}

bool QConsoleDest::isValid()
{
    return true;
}
