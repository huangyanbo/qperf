﻿#ifndef POINTERCALLBACK_H
#define POINTERCALLBACK_H
#include <QtGlobal>
class RequestPacket;
class ReplyPacket;
class StartArgs;
class StopArgs;
class ReadyArgs;

/**
 * @brief The PointerCallback class
 * 表示测量节点的回调抽象。
 */
class PointerCallback
{
public:
    enum State{
        // 未知。
        Unknown,
        // 已连接。
        Connected,
        // 未连接。
        Disconnected
    };
public:
    PointerCallback();
    virtual ~PointerCallback();
    /**
     * @brief onStateChanged 状态改变
     * @param state true表示正常连接，false表示连接断开。
     * @param bCmd  true表示指令链路状态，false表示数据链路状态。
     */
    virtual void onStateChanged(PointerCallback::State state, bool bCmd) = 0;
    /**
     * @brief onStart 接收到开始测试回调。
     */
    virtual void onStart(const StartArgs &args) = 0;
    /**
     * @brief onStop
     */
    virtual void onStop(const StopArgs &args) = 0;
    /**
     * @brief onRequest  接收到请求回调。
     * @param pack
     */
    virtual void onRequest(const RequestPacket &pack) = 0;
    /**
     * @brief onRequest  接收到回复回调。
     * @param pack
     */
    virtual void onReply(const ReplyPacket &pack) = 0;
    /**
     * @brief onReady 接收到reply准备完毕回调。
     * @param args
     */
    virtual void onReady(const ReadyArgs &args) = 0;
};

#endif // POINTERCALLBACK_H
