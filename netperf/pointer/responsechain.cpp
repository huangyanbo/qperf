﻿#include "responsechain.h"

#include <data/commonpack.h>

ResponseChain::ResponseChain(int type, std::function<void(QByteArray)> proceCall)
{
    m_type = type;
    m_proceCall = proceCall;
}

QSharedPointer<ResponseChain> ResponseChain::next() const
{
    return m_next;
}

void ResponseChain::setNext(const QSharedPointer<ResponseChain> &next)
{
    m_next = next;
}

bool ResponseChain::handle(const CommonPack &srcData)
{
    int type = srcData.type();
    if(type == m_type){
        m_proceCall(srcData.content());
        return  true;
    }
    if(!m_next.isNull()){
        return m_next->handle(srcData);
    }
    return false; //  没有下一条处理链。
}
