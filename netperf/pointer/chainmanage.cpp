﻿#include "chainmanage.h"
#include "responsechain.h"

ChainManage::ChainManage()
{

}

void ChainManage::add(QSharedPointer<ResponseChain> chain)
{
    Q_ASSERT(!chain.isNull());

    if(this->m_headerChain.isNull()){
        this->m_headerChain = chain;
    }else{
        this->m_currentChain->setNext(chain);
    }
    this->m_currentChain = chain;
}

bool ChainManage::handle(const CommonPack &srcData)
{
    return this->m_headerChain->handle(srcData);
}
