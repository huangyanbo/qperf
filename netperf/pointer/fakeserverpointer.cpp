﻿#include "fakeserverpointer.h"
#include <QDebug>
#include <QThread>
#include <ipfivetuple.h>
#include <data/requestpacket.h>
#include <data/startargs.h>

FakeServerPointer::FakeServerPointer()
{
}

bool FakeServerPointer::buildDataLink(const IPFiveTuple &args)
{
    Q_UNUSED(args);
    qDebug() << QObject::tr("buildLink!");
    QThread *pThd = QThread::create([&]{ doConnect(false, 1000); } );
    pThd->start();
    return true;
}

void FakeServerPointer::init(const InitArgs &args)
{
    Q_UNUSED(args);
    qDebug() << QObject::tr("init opt execute!");
    QThread *pThd = QThread::create([&]{ doConnect(true, 500); } ); // 创建线程异步执行连接的操作。
    pThd->start();
}

bool FakeServerPointer::start(const StartArgs &args)
{
    Q_UNUSED(args)
    //   （通常为客户端向服务端发送）
    // 虚拟向远端发送开始的请求 。远端收到后，正常流程应该建立连接，返回数据链路正常的状态。
    // 异常情况可能会超时。
    qDebug() << QObject::tr("Send start command to remote!");
    QThread *pThd = QThread::create([&]{ doConnect(false, 800); } );
    pThd->start();
    return true;
}

bool FakeServerPointer::stop(const StopArgs &args)
{
    //    虚拟发送停止请求
    Q_UNUSED(args)
    qDebug() << QObject::tr("Send stop command to remote!");
    return true;
}

void FakeServerPointer::request(const RequestPacket &pack)
{
    Q_UNUSED(pack)
    qDebug() << QObject::tr("Send request command to remote!");
}

void FakeServerPointer::reply(const ReplyPacket &pack)
{
    qDebug() << QObject::tr("Send reply command to remote!");
    Q_UNUSED(pack)
}

void FakeServerPointer::ready()
{
    qDebug() << QObject::tr("Send ready command to remote!");
}

void FakeServerPointer::disConnect(bool bCmd)
{
    qDebug() << QObject::tr("FakePointer::disconnect() !");
    onStateChange(PointerCallback::Disconnected, bCmd);
}

void FakeServerPointer::close(bool bCmd)
{
    qDebug() << QObject::tr("FakePointer::close() !");
    onStateChange(PointerCallback::Disconnected, bCmd);
}

void FakeServerPointer::fakeStart()
{
    QThread::sleep(3); // 等待并启动。
    StartArgs startArgs;
    startArgs.setPackFreq(10);
    startArgs.setTotalCount(30);
    startArgs.setTimeOut(1000);
    startArgs.setPerPackSize(888);
    IPFiveTuple tuple;
    tuple.setType(IPFiveTuple::Type_TcpServer);
    tuple.setLocalAddr(QHostAddress("127.0.0.1"));
    tuple.setLocalPort(12345);
    tuple.setRemoteAddr(QHostAddress("127.0.0.1"));
    tuple.setRemotePort(6666);
    startArgs.setIpTuple(tuple);
    onStart(startArgs);
}

void FakeServerPointer::doConnect(bool bCmd, int waitMTime)
{
    QThread::msleep(uint(waitMTime)); // 等待一段时间后再触发消息。
    onStateChange(PointerCallback::Connected, bCmd);
    if(!bCmd)
    {
        qDebug() << QObject::tr("new client data link connect!");
        QThread::msleep(500);
        RequestPacket pack;
        for (int i = 0; i < 5; i++) {
            qDebug() << QObject::tr("Fake request pack" ) << i;
            pack.setFlag(i);
            onRequest(pack);
            QThread::msleep(1000);
        }
        onStateChange(PointerCallback::Disconnected, bCmd);

        // 重新初始（模拟新的请求）
        QThread *pThd = QThread::create([&]{ doConnect(true, 100); } );
        pThd->start();
        // 模拟新的数据连接。
        QThread *pThd2 = QThread::create([&]{ fakeStart(); });
        pThd2->start();
    }else{
        qDebug() << QObject::tr("new client command link connect!");
    }
}

