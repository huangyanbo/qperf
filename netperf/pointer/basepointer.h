﻿#ifndef BASEPOINTER_H
#define BASEPOINTER_H

#include "pointercallback.h"
#include "threadsafeset.h"

#include <tunnel/tunnelcallback.h>
#include <QVector>
#include <QtGlobal>

class BaseTunnel;
class StartArgs;
class ReplyPacket;
class RequestPacket;
class StopArgs;
class InitArgs;
class IPFiveTuple;

/**
 * @brief The BasePointer class
 *   表示测量的节点。
 */
class BasePointer
{
public:
    BasePointer();
    virtual ~BasePointer();
    /**
     * @brief add 添加数据回调。
     * @param callback 回调的实例。
     */
    void add(PointerCallback *callback);
    /**
     * @brief remove 移除指定回调实例。
     * @param callback  实例。
     */
    void remove(PointerCallback *callback);
public:
    /**
     * @brief buildDataLink 创建数据链路。
     * @param args 五元组参数。
     * @return 失败返回fales。
     */
    virtual bool buildDataLink(const IPFiveTuple &args) = 0;
    /**
     * @brief init 初始化。
     * @param args 初始参数。
     */
    virtual void init(const InitArgs &args) = 0;
    /**
     * @brief start 启动测量。
     * @param args 启动参数。
     * @return 成功发送返回true。
     */
    virtual bool start(const StartArgs &args) = 0;
    /**
     * @brief stop 停止测量。
     * @param args 停止参数。
     * @return
     */
    virtual bool stop(const StopArgs &args) = 0;
    /**
     * @brief request 请求数据包。
     */
    virtual void request(const RequestPacket &pack) = 0;
    /**
     * @brief reply 回复数据包。
     * @param pack 回复包实体。
     */
    virtual void reply(const ReplyPacket &pack) = 0;
    /**
     * @brief ready 回复数据包。
     * @param args 准备完毕数据包实体。
     */
    virtual void ready() = 0;
    /**
     * @brief disConnect 断开当前连接。
     * @param bCmd 是否指令链路。
     */
    virtual void disConnect(bool bCmd) = 0;
    /**
     * @brief close 彻底关闭。
     * @param bCmd 是否指令链路。
     */
    virtual void close(bool bCmd) = 0;
protected:
    /**
     * @brief onStart 启动数据包回调。
     * @param args 启动参数。
     */
    void onStart(const StartArgs &args);
    /**
     * @brief onStop 停止数据包回调。
     * @param args 停止参数。
     */
    void onStop(const StopArgs &args);
    /**
     * @brief onRequest 请求数据包回调。
     */
    void onRequest(const RequestPacket &pack);
    /**
     * @brief onReply 回复数据包回调。
     * @param pack 回复包实体。
     */
    void onReply(const ReplyPacket &pack);
    /**
     * @brief onReady 准备就绪参数回调。
     * @param args 准备就绪参数。
     */
    void onReady(const ReadyArgs &args);
    /**
     * @brief onStateChange  状态改变回调。
     * @param state 当前状态，true表示正常。
     * @param bCmd 是否指令链路。当前两种业务分别是指令链路和数据链路。
     */
    void onStateChange(PointerCallback::State state, bool bCmd);
protected:
    ThreadSafeSet<PointerCallback> m_callbacks; // 回调集合。
    PointerCallback::State m_cmdState;  // 指令链路状态。
    PointerCallback::State m_dataState; // 数据链路状态。
    friend class IntegrationTest;  // 集成测试的类。
    friend class TestServerCtrl;  //服务端控制测试类。
};

#endif // BASEPOINTER_H
