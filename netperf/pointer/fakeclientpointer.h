﻿#ifndef FAKECLIENTPOINTER_H
#define FAKECLIENTPOINTER_H

#include "basepointer.h"


/**
 * @brief The FakeClientPointer class
 * 虚拟的客户端实现。
 */
class FakeClientPointer : public BasePointer
{
public:
    FakeClientPointer();

    // BasePointer interface
public:
    bool buildDataLink(const IPFiveTuple &args)  override;
    void init(const InitArgs &args) override;
    bool start(const StartArgs &args) override;
    bool stop(const StopArgs &args) override;
    void request(const RequestPacket &pack) override;
    void reply(const ReplyPacket &pack) override;
    void ready() override;

    void disConnect(bool bCmd) override;
    void close(bool bCmd) override;
private:
    /**
     * @brief doDataConnect  模拟数据链路连接方法。
     * @param bCmd 是否指令链路。
     * @param waitMTime 操作等待时间。（毫秒）
     */
    void doConnect(bool bCmd, int waitMTime);
    /**
     * @brief doReply 模拟响应。
     * @param flg 标识值。
     */
    void doReply(int flg);
};

#endif // FAKECLIENTPOINTER_H
