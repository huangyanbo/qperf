﻿#include "basepointer.h"
#include "pointercallback.h"
#include <data/startargs.h>
#include <data/requestpacket.h>
#include <QByteArray>
#include <data/replypacket.h>

BasePointer::BasePointer()
{
    m_cmdState = PointerCallback::Unknown;
    m_dataState = PointerCallback::Unknown;
}

BasePointer::~BasePointer()
{

}

void BasePointer::add(PointerCallback *callback)
{
    if(callback)
    {
        //   推送当前状态(当状态不为Unknown时才推送）
        if(m_dataState != PointerCallback::Unknown)
        {
            callback->onStateChanged(m_dataState, false);
        }
        if(m_cmdState != PointerCallback::Unknown)
        {
            callback->onStateChanged(m_cmdState, true);
        }
        m_callbacks.add(callback);
    }
}

void BasePointer::remove(PointerCallback *callback)
{
    m_callbacks.remove(callback);
}

void BasePointer::onStart(const StartArgs &args)
{
    auto func = [&](PointerCallback *item)->void {item->onStart(args);};
    m_callbacks.execEach(func);
}

void BasePointer::onStop(const StopArgs &args)
{
    auto func = [&](PointerCallback *item)->void {item->onStop(args);};
    m_callbacks.execEach(func);
}

void BasePointer::onRequest(const RequestPacket &pack)
{
    auto func = [&](PointerCallback *item)->void {item->onRequest(pack);};
    m_callbacks.execEach(func);
}

void BasePointer::onReply(const ReplyPacket &pack)
{
    auto func = [&](PointerCallback *item)->void {item->onReply(pack);};
    m_callbacks.execEach(func);
}

void BasePointer::onReady(const ReadyArgs &args)
{
    auto func = [&](PointerCallback *item)->void {item->onReady(args);};
    m_callbacks.execEach(func);
}

void BasePointer::onStateChange(PointerCallback::State state, bool bCmd)
{
    if(bCmd)
    {
        m_cmdState = state;
    }else {
        m_dataState = state;
    }
    auto func = [&](PointerCallback *item)
            ->void {item->onStateChanged(state, bCmd);};
    m_callbacks.execEach(func);
}
