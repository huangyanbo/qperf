﻿#include "testclientctrl.h"

#include <pointer/pointerfactory.h>
#include <context/contextfactory.h>
#include <context/context.h>
#include <QsLog.h>

TestClientCtrl::TestClientCtrl(const InitArgs &initArgs, const StartArgs &startArgs) :
    m_initArgs (initArgs),
    m_startArgs(startArgs)
{
}

bool TestClientCtrl::start()
{
    // 创建Pointer实例。
    PointerFactory pointerFact(false, true);
    m_pointer = pointerFact.create();
    // 初始。
    ContextFactory fact(m_pointer);
    fact.setArgs(m_startArgs);

    m_context = fact.create();
    m_context->init(m_initArgs);
    QLOG_INFO() << QObject::tr(" start client now...");
    return true;
}
