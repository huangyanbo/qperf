﻿#include "ctrlfactory.h"
#include "ctrlfactoryproducer.h"
#include "fakectrlfactory.h"

CtrlFactoryProducer::CtrlFactoryProducer()
{
    m_bServer = true;
    m_bFake = false;
}

CtrlFactoryProducer::CtrlFactoryProducer(const StartArgs &args)
{
    m_bServer = false;
    m_bFake = false;
    m_args = args;
}

CtrlFactoryProducer::CtrlFactoryProducer(bool bServer, const StartArgs &args)
{
    m_bServer = bServer;
    m_args = args;
    m_bFake = true;
}

CtrlFactoryProducer::CtrlFactoryProducer(bool bServer, bool bFake, const StartArgs &args)
{
    m_bServer = bServer;
    m_bFake = bFake;
    m_args = args;
}

AbstractCtrlFactory *CtrlFactoryProducer::create()
{
    if(m_bFake)
    {
        return new FakeCtrlFactory(m_bServer, m_args);
    }
    if(m_bServer)
    {
        return new CtrlFactory();
    }else {
        return  new CtrlFactory(m_args);
    }
}

