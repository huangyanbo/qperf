﻿#include "clientctrl.h"

#include <pointer/pointerfactory.h>
#include <pointer/basepointer.h>
#include <context/context.h>
#include <context/contextfactory.h>
#include "QsLog.h"

ClientCtrl::ClientCtrl(const InitArgs &initArgs, const StartArgs &startArgs)
    :m_initArgs (initArgs),
     m_startArgs(startArgs)
{
}

bool ClientCtrl::start()
{
    IPFiveTuple::Type type = m_initArgs.tuple().type() ;
    Q_ASSERT(type == IPFiveTuple::Type_TcpClient); // 必须是TcpClient作为客户端。

    PointerFactory pointerFact(false, false);
    m_pointer = pointerFact.create();
    if(!m_pointer)
    {
        return false;
    }

    // 初始。
    ContextFactory fact(m_pointer);
    fact.setArgs(m_startArgs);
    m_context = fact.create();
    if(!m_context) // 初始失败，删除跑路。
    {
        delete m_pointer;
        m_pointer = Q_NULLPTR;
        return false;
    }
    m_context->init(m_initArgs);
    connect(m_context, &Context::destroyed, this, &ClientCtrl::exit);
    connect(m_context, &Context::workProcess, this, &ClientCtrl::workProcess);
    return true;
}

void ClientCtrl::workProcess(float percent)
{
    QLOG_INFO() << QString(tr("Work progress :%1%.")).arg(QString::number(percent * 100, 'f', 2));
}

