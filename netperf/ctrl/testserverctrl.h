﻿#ifndef TESTSERVERCTRL_H
#define TESTSERVERCTRL_H

#include "basectrl.h"

#include <data/initargs.h>
#include <data/startargs.h>

/**
 * @brief The TestServerCtrl class
 *  测试服务端。
 */
class TestServerCtrl : public BaseCtrl
{
public:
    /**
     * @brief TestServerCtrl
     * @param initArgs 初始参数。
     * @param startArgs 模拟接收到客户端的启动参数。
     */
    TestServerCtrl(const InitArgs &initArgs, const StartArgs startArgs);

    // BaseCtrl interface
public:
    bool start() override;
private:
    InitArgs m_initArgs;
    StartArgs m_startArgs;
};

#endif // TESTSERVERCTRL_H
