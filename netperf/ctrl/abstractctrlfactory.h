﻿#ifndef ABSTRACTCTRLFACTORY_H
#define ABSTRACTCTRLFACTORY_H

#include <data/startargs.h>

class BaseCtrl;
class InitArgs;

/**
 * @brief The AbstractCtrlFactory class
 *  抽象控制器工厂。
 */
class AbstractCtrlFactory
{
public:
    AbstractCtrlFactory();
    AbstractCtrlFactory(const StartArgs &startArgs);
    virtual ~AbstractCtrlFactory();
    virtual BaseCtrl *create(const InitArgs &initArgs) = 0;
protected:
    StartArgs m_startArgs; // 客户端初始参数。
    bool m_bServer;  // 是否服务端。
};

#endif // ABSTRACTCTRLFACTORY_H
