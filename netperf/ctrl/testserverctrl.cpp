﻿#include "testserverctrl.h"

#include <pointer/pointerfactory.h>
#include <pointer/basepointer.h>

#include <context/context.h>
#include <context/contextfactory.h>

#include <data/startargs.h>

#include <QThread>

TestServerCtrl::TestServerCtrl(const InitArgs &initArgs, const StartArgs startArgs):
    m_initArgs (initArgs),
    m_startArgs(startArgs)
{
}

bool TestServerCtrl::start()
{
    // 创建Pointer实例。
    PointerFactory pointerFact(true, true);
    m_pointer = pointerFact.create();

    // 初始。
    ContextFactory fact(m_pointer);
    m_context = fact.create();
    m_context->init(m_initArgs);
    // 服务端模拟数据链接。
    qDebug() << QObject::tr(" start server now...");

    QThread::sleep(3); // 等待并启动。
    m_pointer->onStart(m_startArgs);
    return true;
}
