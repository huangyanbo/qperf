﻿#ifndef BASECTRL_H
#define BASECTRL_H

#include <QObject>
class Context;
class BasePointer;
class InitArgs;
class StartArgs;
/**
 * @brief The PerfCtrl class
 * 表示性能测试控制器基类。
 */
class BaseCtrl : public QObject
{
    Q_OBJECT
public:
    explicit BaseCtrl(QObject *parent = Q_NULLPTR);
    virtual ~BaseCtrl() override;
    /**
     * @brief start 启动。
     * @return
     */
    virtual bool start() = 0;
protected slots:
    void exit();
protected:
    Context *m_context; // 上下文实例。
    BasePointer *m_pointer; // 远端节点的实例。
};

#endif // BASECTRL_H
