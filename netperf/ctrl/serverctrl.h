﻿#ifndef SERVERCTRL_H
#define SERVERCTRL_H

#include "basectrl.h"

#include <data/initargs.h>

/**
 * @brief The ServerCtrl class
 * 服务端控制逻辑。
 */
class ServerCtrl : public BaseCtrl
{
public:
    ServerCtrl(const InitArgs &initArgs);

    // BaseCtrl interface
public:
    bool start() override;
private:
    InitArgs m_initArgs;
};

#endif // SERVERCTRL_H
