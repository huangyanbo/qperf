﻿#include "clientctrl.h"
#include "ctrlfactory.h"
#include "serverctrl.h"

CtrlFactory::CtrlFactory()
    : AbstractCtrlFactory ()
{
}

CtrlFactory::CtrlFactory(const StartArgs &startArgs)
    :AbstractCtrlFactory (startArgs)
{
}

BaseCtrl *CtrlFactory::create(const InitArgs &initArgs)
{
    if(m_bServer)
    {
        return new ServerCtrl(initArgs);
    }
    return new ClientCtrl(initArgs, m_startArgs);
}
