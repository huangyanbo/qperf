﻿#ifndef FAKECTRLFACTORY_H
#define FAKECTRLFACTORY_H

#include "abstractctrlfactory.h"

/**
 * @brief The FakeCtrlFactory class
 *  虚拟控制器工厂实现。
 */
class FakeCtrlFactory: public AbstractCtrlFactory
{
public:
    /**
     * @brief FakeCtrlFactory
     * @param bServer 是否服务端 。
     * @param startArgs 启动参数（客户端）；模拟启动参数（服务端）。
     */
    FakeCtrlFactory(bool bServer, const StartArgs &startArgs);
    BaseCtrl *create(const InitArgs &initArgs) override;
};

#endif // FAKECTRLFACTORY_H
