﻿#include "serverctrl.h"

#include <pointer/pointerfactory.h>
#include <pointer/basepointer.h>

#include <context/context.h>
#include <context/contextfactory.h>

ServerCtrl::ServerCtrl(const InitArgs &initArgs)
    :BaseCtrl (Q_NULLPTR),
      m_initArgs(initArgs)
{
}

bool ServerCtrl::start()
{
    IPFiveTuple::Type type = m_initArgs.tuple().type() ;
    Q_ASSERT(type == IPFiveTuple::Type_TcpServer); // 必须是TcpServer作为客户端。

    PointerFactory pointerFact(true, false);
    m_pointer = pointerFact.create();
    if(!m_pointer)
    {
        return false;
    }

    // 初始。
    ContextFactory fact(m_pointer);
    m_context = fact.create();
    if(!m_context) // 初始失败，删除跑路。
    {
        delete m_pointer;
        m_pointer = Q_NULLPTR;
        return false;
    }
    connect(m_context, &Context::destroyed, this, &ServerCtrl::exit);
    m_context->init(m_initArgs);
    return true;
}
