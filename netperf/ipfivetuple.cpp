﻿#include "ipfivetuple.h"

IPFiveTuple::IPFiveTuple()
{
    m_localPort = 0;
    m_remotePort = 0;
    m_type = Type_Udp;
}

IPFiveTuple::IPFiveTuple(IPFiveTuple::Type type, QHostAddress &remoteAddr, quint16 remotePort, QHostAddress &localAddr, quint16 localPort)
{
    m_type = type;
    m_remoteAddr = remoteAddr;
    m_remotePort = remotePort;
    m_localAddr = localAddr;
    m_localPort = localPort;
}

IPFiveTuple::Type IPFiveTuple::type() const
{
    return m_type;
}

void IPFiveTuple::setType(const IPFiveTuple::Type &type)
{
    m_type = type;
}

quint16 IPFiveTuple::remotePort() const
{
    return m_remotePort;
}

void IPFiveTuple::setRemotePort(const quint16 &remotePort)
{
    m_remotePort = remotePort;
}

QHostAddress IPFiveTuple::localAddr() const
{
    return m_localAddr;
}

void IPFiveTuple::setLocalAddr(const QHostAddress &localAddr)
{
    m_localAddr = localAddr;
}

quint16 IPFiveTuple::localPort() const
{
    return m_localPort;
}

void IPFiveTuple::setLocalPort(const quint16 &localPort)
{
    m_localPort = localPort;
}

QHostAddress IPFiveTuple::remoteAddr() const
{
    return m_remoteAddr;
}

void IPFiveTuple::setRemoteAddr(const QHostAddress &remoteAddr)
{
    m_remoteAddr = remoteAddr;
}

void IPFiveTuple::reverse()
{
    QHostAddress tmp = m_remoteAddr;
    m_remoteAddr = m_localAddr;
    m_localAddr = tmp;

    quint16 tmpPort = m_remotePort;
    m_remotePort = m_localPort;
    m_localPort = tmpPort;
    switch (type()) {
    case IPFiveTuple::Type_Udp: // udp 不改变。
        break;
    case IPFiveTuple::Type_TcpServer: // Server/Client 互换。
        setType(Type_TcpClient);
        break;
    case IPFiveTuple::Type_TcpClient:
        setType(Type_TcpClient);
        break;
    }
}

QString IPFiveTuple::toString() const
{
    QString src;
    QTextStream stream(&src);
    QString type;
    switch(m_type)
    {
    case IPFiveTuple::Type_Udp:
        type = "UDP";
        break;
    case IPFiveTuple::Type_TcpServer:
        type = "TcpServer";
        break;
    case IPFiveTuple::Type_TcpClient:
        type = "TcpClient";
        break;
    }

    stream << QObject::tr("type:") << type
           << "," << QObject::tr(" local addr:") << m_localAddr.toString()
           << "," << QObject::tr(" local port:") << m_localPort
           << "," << QObject::tr(" remote addr:") << m_remoteAddr.toString()
           << "," << QObject::tr(" remote port:") << m_remotePort;
    return src;
}
