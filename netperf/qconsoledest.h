﻿#ifndef QDEBUGOUTPUTDEST_H
#define QDEBUGOUTPUTDEST_H

#include <QsLogDest.h>

using namespace  QsLogging;
/**
 * @brief The QConsoleDest class
 *  qt控制台输出。
 */
class QConsoleDest : public Destination
{
public:
    QConsoleDest();

    // Destination interface
public:
    void write(const QString &message, Level level) override;
    bool isValid() override;
};

#endif // QDEBUGOUTPUTDEST_H
