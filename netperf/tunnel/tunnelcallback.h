﻿#ifndef TUNNELCALLBACK_H
#define TUNNELCALLBACK_H

class QByteArray;

/**
 * @brief The TunnelCallback class
 *  通道回调。
 */
class TunnelCallback
{
public:
    TunnelCallback();
    virtual ~TunnelCallback();
    /**
     * @brief onProc 接收数据通知。
     * @param data
     */
    virtual void onProc(const QByteArray &data) = 0;
    /**
     * @brief onStateChange 状态改变通知。
     * @param state 状态，true表示正常。
     */
    virtual void onStateChange(bool state) = 0;
};

#endif // TUNNELCALLBACK_H
