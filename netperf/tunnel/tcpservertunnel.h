﻿#ifndef TCPSERVERTUNNEL_H
#define TCPSERVERTUNNEL_H

#include "basetunnel.h"

#include <ipfivetuple.h>
#include <qnet_global.h>

#include <net/netreading.h>
#include <net/tcpsession.h>

QNET_NAMESPACE_BEGIN
class TcpServer;
QNET_NAMESPACE_END

QNET_USING_NAMESPACE
/**
 * @brief The TcpTunnel class
 * tcp  服务端隧道
 */
class TcpServerTunnel :
        public BaseTunnel,
        public ISessionListener,
        public NetReading
{
public:
    TcpServerTunnel(const IPFiveTuple &ipTuple);
    ~TcpServerTunnel() override;

    // BaseTunnel interface
public:
    bool build() override;
    void destory() override;
    int write(QByteArray &src) override;
    bool state() override;
    void disConnect() override;
    void fetchTuple(IPFiveTuple &tuple) override;

    // ISessionListener interface
public:
    void onStateChanged(TcpSession *session, bool connected) override;
    void onError(QAbstractSocket::SocketError) override;

    // NetReading interface
public:
    void onProc(const QByteArray &arry) override;

private:
    TcpServer *m_server;
    TcpSession *m_session;
    IPFiveTuple m_ipTuple;
    bool m_state;
    QThread *m_innerThread;
};

#endif // TCPTUNNEL_H
