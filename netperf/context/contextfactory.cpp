﻿#include "context.h"
#include "contextfactory.h"

#include <data/startargs.h>


ContextFactory::ContextFactory(BasePointer *pointer)
{
    m_pointer = pointer;
    m_startArgs = Q_NULLPTR;
    m_bServer = true;
}

void ContextFactory::setArgs(const StartArgs &args)
{
    if(m_startArgs)
    {
        delete m_startArgs;
    }
    m_startArgs = new StartArgs(args);
    m_bServer = false;
}

ContextFactory::~ContextFactory()
{
    if(m_startArgs)
    {
        delete m_startArgs;
        m_startArgs = Q_NULLPTR;
    }
}

Context *ContextFactory::create()
{
    //  创建服务端上下文或客户端上下文。
    if(m_bServer)
    {
        return new Context(m_pointer);
    }
    return new Context(m_pointer, *m_startArgs);
}
