﻿#include "integrationtest.h"

#include <data/initargs.h>
#include <data/startargs.h>

#include <pointer/basepointer.h>
#include <pointer/pointerfactory.h>

#include <context/contextfactory.h>
#include <context/context.h>

#include <QThread>

IntegrationTest::IntegrationTest()
{
    m_context = Q_NULLPTR;
    m_pointer = Q_NULLPTR;
}

IntegrationTest::~IntegrationTest()
{
    if(m_context)
    {
        delete m_context;
        m_context = Q_NULLPTR;
    }
    if(m_pointer)
    {
        delete m_pointer;
        m_pointer = Q_NULLPTR;
    }
}

void IntegrationTest::run(bool bServer)
{
    // 创建Pointer实例。
    InitArgs initArgs;
    PointerFactory pointerFact(bServer, true);
    m_pointer = pointerFact.create();

    StartArgs startArgs;
    startArgs.setPackFreq(50);
    startArgs.setTotalCount(1000);
    startArgs.setTimeOut(1000);
    startArgs.setPerPackSize(888);
    // 启动。
    IPFiveTuple tuple;
    tuple.setType(IPFiveTuple::Type_TcpServer);
    tuple.setLocalAddr(QHostAddress("127.0.0.1"));
    tuple.setLocalPort(12345);
    tuple.setRemoteAddr(QHostAddress("127.0.0.1"));
    tuple.setRemotePort(6666);
    startArgs.setIpTuple(tuple);

    // 初始。
    ContextFactory fact(m_pointer);
    if(!bServer)
    {
       fact.setArgs(startArgs);
    }
    m_context = fact.create();
    m_context->init(initArgs);
    if(bServer)  // 服务端模拟数据链接。
    {
        qDebug() << QObject::tr(" start server now...");
        QThread::sleep(3); // 等待并启动。
        m_pointer->onStart(startArgs);
    }else { // 客户端发送启动指令。
        qDebug() << QObject::tr(" start client now...");
//        m_context->start(startArgs);
    }

}
