# QPerf  

``` 
   ___  ____            __      _                               _ 
  / _ \|  _ \ ___ _ __ / _|    | |__  _   _       _ __   ___ __| |
 | | | | |_) / _ \ '__| |_     | '_ \| | | |     | '_ \ / __/ _` |
 | |_| |  __/  __/ |  |  _|    | |_) | |_| |     | |_) | (_| (_| |
  \__\_\_|   \___|_|  |_|      |_.__/ \__, |     | .__/ \___\__,_|
                                      |___/      |_|               
     
 ```
  
如果项目帮助到你，请点个星星。  
如果你想加入项目，请私信作者或留言。  
如果你发现了项目的BUG，请提交`PR` 或 `Issue`  。  
项目及文档持续完善中，欢迎加入。交流QQ群：972641491。    

[历史版本](https://gitee.com/andwp/qperf/releases)

[![pcd/QPerf](https://gitee.com/andwp/qperf/widgets/widget_card.svg?colors=393222,ebdfc1,fffae5,d8ca9f,393222,a28b40)](https://gitee.com/andwp/qperf) 

  
---

## :fire: 介绍
QPerf是跨平台网络性能测试软件，采用Qt5框架开发。与其它同类工具不同，QPerf支持使用Udp或Tcp在极限情况下测量网络状况。      
### :construction: 应用场景  
1. `网络丢包测试`，想测量网络中两台主机在特定发包速率下的丢包率，用`QPerf`；  
2. `网络极限测试`，想测量网络中两台主机的tcp或udp发包的极限，用`QPerf`；
3. 无聊就想`试试`，用`QPerf`。
### :alien: 引用的开源项目     
* [QtNet](https://gitee.com/andwp/qt-socket)  `这是一个基于Qt5的开源网络库，封装Udp、Tcp的基础通信。`
* [QsLog](https://bitbucket.org/razvanpetru/qt-components/wiki/QsLog)  `这是一个基于Qt开发的开源日志库，封装基本的日志操作。`
### :rocket:使用方法
```
Usage: qperf  [options]
QPerf --- a tool for measure network status .

Options:
  -?, -h, --help             Displays this help.[显示帮助。]
  -v, --version              Displays version information.[版本。]
  -p, --cport <port>         Command link port, <port>.[命令链路使用端口。]
  -a, --caddr <address>      Command link address, <address>.[命令链路使用地址，客户端必填。]
  -P, --dport <port>         Data link port.[数据链路使用端口。]
  -s, --server               Run with server mode,default in client mode.[是否服务端，服务端选项，默认客户端。]
  -u, --udp                  Data link use udp.  [数据链路是否使用UDP模式。]
  -t, --timout <timeout>     Timeout (ms). [数据超时时间，默认1000（ms），服务端设置无效。]
  -r, --frequency <freq>     Send packet frequency per second. [发送数据频率，服务端设置无效。]
  -o, --total <freq>         Total packet send. [发送数据总数，服务端设置无效。]
  -S, --packSize <packSize>  Send perpacket size(in bytes). [发送数据包大小（字节）。]  
  -w, --warm <warm packet size>  Test warm packet, default 10.[测试热身数据包，默认10。]
  -f, --fake                 Run progress in fake .[是否使用测试模式。调试使用。]

```
---  
#### :lock: 使用例子 
* :rocket: 创建服务端,监听本地5555端口。
```
./qperf -s -a 127.0.0.1 -p 5555
```
* :rocket: 创建客户端端，连接服务端端，并以6666端口发送数据,数据按100包/秒的频率发送1000包，每包数据大小200字节。
```
./qperf -a 127.0.0.1 -p 5555 -P 6666 -r 100 -o 1000 -S 200
```

### :construction: 编译说明  
QPerf支持在Windows 和Linux 操作系统上编译。
#### :art: 命令行编译 
```
qmake.exe  qperf.pro -spec win32-g++ "CONFIG+=release"  &&  mingw32-make.exe qmake_all  
mingw32-make.exe -j8 
``` 
---
#### :art: 使用集成环境编译   
`测试支持Qt5.12及以上的编译环境。  `   
1. 打开QtCreator，设置编译器；
2. 编译qtsocket项目；
3. 编译netperf项目；
4. 运行netperf项目。
## :fire:软件架构
见[软件架构](doc/架构与设计.md)
## :fire:目录结构
- qtnet QtNet通信库(采用git subtree引用）；  
- netperf  网络性能测试工具；  
- perfunittest 性能测试工具的单元测试；  
- QsLog QsLog日志库；
- doc 文档相关。

## :wrench:编码规范  
见[编码规范](doc/编码规范.md)
